Remark: yes I am aware that there is unsafe code in this repo : ) this is just a repo for testing purposes ; )

Constraints:

- please start the project with git init so I can see your commits when checking your code
- use of NextJS
- use of tailwindcss
- use of google firebase
- use of react tsx
- use of typescript
- use of redux for any state management

Assignment 1 description

- create a file that generates a ETH-compatible wallet.
- Please make a token presale page : /presale . Please ignore anything that is style related. It does not need to look pretty
- The presale page should present the user with the option to buy tokens
- there should be a form with 1 field input: value in $USDC. We sell tokens for $0.0003 per token, and we are currently in Round one of the presale
- the ETH should be send to the wallet that we generated in step 1
- create an info popup that shows any errors, but which also shows a success message when a user successfully buys tokens
- the back end should make sure that the purchase date, transaction amount, presale round number, $USDC value of the purchase, and token amount (of the token we are offering) get stored to the database whenever a transaction succeeds

Assignment 2

- please solve the algorithmic challenge in /algochallenge.ts
