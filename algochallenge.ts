type StringObject = { [key: string]: string };

export type UnpredictableObject = {
  a: number;
  c: any;
  b?: StringObject;
};

// please create a function with returntype Array<StringObject> | null,  that does the following:
// - takes a minimum of 2 parameters: one parameter is called checkObject and is of type StringObject. The other parameter is of the type UnpredictableObject. (You can assume that the UnpredictableObject
//   can have 0 or 1 arrays somehwere inside of it)
// - finds any array anywhere inside the UnpredictableObject that has xyz as a key. for example, this could be the case:

const obj: UnpredictableObject = {
  a: 22,
  c: {
    xcd: {
      ddffd: {
        xxxsdsd: [
          {
            a: 343,
            c: "blabla",
            d: 344,
          },
        ],
      },
    },
  },
};
// - if the array is not found, your function returns null
// - if the array is found, it returns a filtered version of the array. The filtered array only has objects
//   that contain all key value pairs that are present inside the StringObject that was provided to your function as a first argument (parameter called checkObject)

function findFilteredArray(
  checkObject: StringObject,
  unpredictableObject: UnpredictableObject
): Array<StringObject> | null {
  let targetArray: any[] | null = null;

  function findArray(obj: any) {
    if (Array.isArray(obj)) {
      const hasXyzKey = obj.some((item) => item.xyz !== undefined);
      if (hasXyzKey) {
        targetArray = obj;
        return;
      }
    }
    if (typeof obj === "object" && obj !== null) {
      Object.values(obj).forEach((val) => findArray(val));
    }
  }

  findArray(unpredictableObject);

  if (targetArray === null) {
    return null;
  }

  const filteredArray = targetArray.filter((item: any) => {
    const keys = Object.keys(checkObject);
    return keys.every((key) => item[key] === checkObject[key]);
  });

  return filteredArray;
}


// Test case 1: array not found, should return null
const checkObject1 = { a: "123", c: "blabla" };
const unpredictableObject1 = {
  a: 22,
  c: {
    xcd: {
      ddffd: {
        abcsdsd: [
          {
            a: "123",
            c: "blabla",
            d: 344,
          },
        ],
      },
    },
  },
};
console.log(findFilteredArray(checkObject1, unpredictableObject1)); // should output null

// Test case 2: array found but no matching objects, should return empty array
const checkObject2 = { a: "123", c: "blabla" };
const unpredictableObject2 = {
  a: 22,
  c: {
    xcd: {
      ddffd: {
        xyzsdsd: [
          {
            a: 343,
            c: "hello",
            d: 344,
          },
        ],
      },
    },
  },
};
console.log(findFilteredArray(checkObject2, unpredictableObject2)); // should output []

// Test case 3: array found and some objects match, should return filtered array
const checkObject3 = { a: "123", c: "blabla" };
const unpredictableObject3 = {
  a: 22,
  c: {
    xcd: {
      ddffd: {
        xyzsdsd: [
          {
            a: "123",
            c: "blabla",
            d: 344,
          },
          {
            a: "123",
            c: "blabla",
            d: 345,
          },
          {
            a: "456",
            c: "blabla",
            d: 346,
          },
        ],
      },
    },
  },
};
console.log(findFilteredArray(checkObject3, unpredictableObject3));
// should output:
// [
//   { a: "123", c: "blabla", d: 344 },
//   { a: "123", c: "blabla", d: 345 }
// ]

// Test case 4: array found and all objects match, should return entire array
const checkObject4 = { a: "123", c: "blabla", d: 344 };
const unpredictableObject4 = {
  a: 22,
  c: {
    xcd: {
      ddffd: {
        xyzsdsd: [
          {
            a: "123",
            c: "blabla",
            d: 344,
          },
          {
            a: "123",
            c: "blabla",
            d: 344,
          },
        ],
      },
    },
  },
};
console.log(findFilteredArray(checkObject4, unpredictableObject4));
// should output:
// [
//   { a: "123", c: "blabla", d: 344 },
//   { a: "123", c: "blabla", d: 344 }
// ]
