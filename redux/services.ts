// import Web3EthContract from "web3-eth-contract";
import { actions } from "./store";
import { MetaMaskInpageProvider } from "@metamask/providers";

declare global {
    interface Window {
        ethereum: MetaMaskInpageProvider;
    }
}

const NETWORK = {
    ID: 1,
    NAME: "Ethereum Mainnet"
};

export const connectMetaMaskWallet = async (dispatch: any) => {
    dispatch(actions.connectRequest({}));
    const { ethereum } = window;
    const metamaskIsInstalled = ethereum && ethereum.isMetaMask;
    if (metamaskIsInstalled) {
        try {
            const accounts = await ethereum.request<string[]>({
                method: "eth_requestAccounts",
            });
            const networkId = await ethereum.request<string>({
                method: "net_version",
            });
            if (parseInt(networkId!) === NETWORK.ID) {
                dispatch(
                    actions.connected({
                        account: accounts && accounts[0],
                    })
                );
                // Add listeners start
                ethereum.on("accountsChanged", (accounts) => {
                    const strAccounts = accounts as string[];
                    dispatch(actions.connected({ account: strAccounts[0] }));
                });
                ethereum.on("chainChanged", () => {
                    window.location.reload();
                });
                // Add listeners end
            } else {
                dispatch(actions.connectFailed(`Change network to ${NETWORK.NAME}.`));
            }
        } catch (error) {
            dispatch(actions.connectFailed("Something went wrong."));
        }
    } else {
        dispatch(actions.connectFailed("Install Metamask."));
    }
};
