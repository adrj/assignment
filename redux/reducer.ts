export interface State {
  account: string | null
}

export const reducer = (
  state: State = { account: null },
  action: any
) => {
  switch (action.type) {
    case "LOGIN":
      return {
        ...state,
        account: action.payload.account,
      };
    case "LOGOUT":
      return {
        ...state,
        account: null,
      };
    default:
      return state;
  }
};
