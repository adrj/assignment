import { initializeApp } from "firebase/app";
import {
  DocumentData,
  getDoc,
  collection,
  where,
  getFirestore,
  PartialWithFieldValue,
  getDocs,
  doc,
  setDoc,
  query,
  Query,
} from "firebase/firestore";

import { StringObject, User } from "./types";

import { nanoid } from "nanoid";

export const getAPIURL = (): string => {
  return process.env.NODE_ENV === "development"
    ? "http://localhost:3000/api"
    : "";
};

export const getDBConfig = () => {
  return {
    apiKey: "AIzaSyDgU-4PcbAfJ8nadD0gqsHLZ4d6jTnUQkI",
    authDomain: "testdev-db1a1.firebaseapp.com",
    projectId: "testdev-db1a1",
    storageBucket: "testdev-db1a1.appspot.com",
    messagingSenderId: "255138138134",
    appId: "1:255138138134:web:027747fe0321d4a71b8d37",
    measurementId: "G-BRV9V0RQY3",
  };
};

export default function loadFirestore() {
  const app = initializeApp(getDBConfig());
  const db = getFirestore(app);

  return { app, db };
}

const { db } = loadFirestore();

export async function saveAndGetDocument<Type>(
  collectionName: string,
  object: Type & { id?: string }
) {
  const id = object.id || nanoid();

  const ref = doc(db, collectionName, id);

  await setDoc(
    ref,
    { ...object, id },
    {
      merge: true,
    }
  );

  const document = doc(db, collectionName, `${id}`);

  const docSnap = await getDoc(document);

  const updated = await docSnap.data();

  return {
    ...updated,
    created: jsonDate(updated?.created),
    signin: jsonDate(updated?.signin),
  } as Type;
}

export async function getDocuments<Type>(
  collectionName: string,
  equals?: StringObject
): Promise<Array<Type>> {
  return new Promise(async (resolve, reject) => {
    const querySnapshot = await getDocs(
      Object.keys(equals || []).reduce((accu: Query, el): Query => {
        return query(accu, where(el, "==", equals![el]));
      }, query(collection(db, collectionName)))
    );

    const res: any = [];
    querySnapshot.forEach((doc) => {
      res.push(doc.data());
    });

    return resolve(res);
  });
}

export const jsonDate = (timestamp: { toDate?: () => Date }) =>
  timestamp?.toDate ? timestamp.toDate().toJSON() : "";

export const cleanTimeOut = async (timer: number) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      return resolve("success");
    }, timer);
  });
};
