
import React, { useState } from 'react';
import { generateWallet } from '../utils/wallet';
import { ethers } from "ethers";
import { db } from "../utils/firebase";

const Presale = () => {
  const [usdcValue, setUsdcValue] = useState<number>(0);

  async function handleBuyTokens() {
    const wallet = await generateWallet();
    // Send the ETH to the wallet
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    const signer = provider.getSigner();
    const transaction = {
      to: await wallet.address,
      value: ethers.utils.parseEther((usdcValue / 0.0003).toString()), // Convert USDC value to ETH value
    };
    await signer.sendTransaction(transaction);

    // Store the purchase details in the database
    const purchaseDetails = {
      purchaseDate: new Date(),
      transactionAmount: transaction.value.toString(),
      presaleRoundNumber: 1,
      usdcValue,
      tokenAmount: usdcValue / 0.0003,
    };

    await db.collection('purchases').add(purchaseDetails);
  }

  return (
    <div className="absolute  h-[100vh] w-[100vw] flex flex-col items-center justify-center">
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold">Token Presale</h1>
      <div className="mt-4">
        <label htmlFor="usdcValue" className="block font-medium text-gray-700">
          USDC Value:
        </label>
        <input
          id="usdcValue"
          type="number"
          value={usdcValue}
          onChange={(e) => setUsdcValue(parseFloat(e.target.value))}
          className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 focus:ring-offset-0 focus:ring-2"
        />
      </div>
      <button
        onClick={handleBuyTokens}
        className="mt-4 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
      >
        Buy Tokens
      </button>
    </div>
    </div>
  );
}

export default Presale;