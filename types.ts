export type State = {
  account: string | null;
  loading: boolean;
  error: string | null;
};

export type User = {
  id: string;
  address?: string;
  created: string;
  name?: string;
  notifyIds?: string[];
  picture?: string;
  nonce?: string;
  signin: string | null;
  spinPlays: number;
};

export type StringObject = { [key: string]: string };
