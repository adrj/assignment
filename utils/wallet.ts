import { ethers } from 'ethers';

export const generateWallet = async () => {
  const wallet = ethers.Wallet.createRandom();
  return {
    privateKey: wallet.privateKey,
    address: wallet.address,
  };
};
